# OpenML dataset: King-rook-vs-King

https://www.openml.org/d/46173

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**King-Rock-vs-King** (kr-vs-k) dataset. Source: **KEEL**

**Abstract**: A chess endgame data set representing the positions on the board of the white king, the white rook, and the black king. The task is to determine the optimum number of turn required for white to win the game, which can be a draw if it takes more than sixteen turns. 

**Attributes Details**: 
- White_king_col {a, b, c, d, e, f, g, h} 
- White_king_row {1, 2, 3, 4, 5, 6, 7, 8} 
- White_rook_col {a, b, c, d, e, f, g, h} 
- White_rook_row {1, 2, 3, 4, 5, 6, 7, 8} 
- Black_king_col {a, b, c, d, e, f, g, h} 
- Black_king_row {1, 2, 3, 4, 5, 6, 7, 8} 
- Class - Game {draw, zero, one, two, three, four, five, six, seven, eight, nine, ten, eleven, twelve, thirteen, fourteen, fifteen, sixteen}

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/46173) of an [OpenML dataset](https://www.openml.org/d/46173). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/46173/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/46173/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/46173/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

